# -*- coding: utf-8 -*-
import random
import torch

N, D_in = 64, 1000

x = torch.randn(N, D_in)
y = torch.randn(N, D_in)

loss_fn = torch.nn.MSELoss()
loss = loss_fn(x, y)
print 'average loss: ' + str(loss.item())

loss_fn = torch.nn.MSELoss(size_average=False)
loss = loss_fn(x, y)
print 'loss ' + str(loss.item())
