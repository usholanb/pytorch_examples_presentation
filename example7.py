# -*- coding: utf-8 -*-
import torch
import random
import numpy as np
from sklearn import datasets
import torch.nn.functional as F


# N is batch size; D_in is input dimension;
# H is hidden dimension; D_out is output dimension.
D_in, H, D_out = 64, 100, 10


dataset = datasets.load_digits(n_class=10, return_X_y=False)

x = dataset.data
y = dataset.target
print 'x.shape: {}'.format(x.shape)
print 'y.shape: {}'.format(y.shape)
x, y = torch.from_numpy(x).type(torch.float), torch.from_numpy(y).type(torch.long)


# Use the nn package to define our model and loss function.
class ThreeLayerNet(torch.nn.Module):

    def __init__(self, D_in, H, D_out):
        super(ThreeLayerNet, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.linear2 = torch.nn.Linear(H, H)
        self.linear3 = torch.nn.Linear(H, D_out)
        self.relu = torch.nn.ReLU()

    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h1_relu = self.relu(self.linear1(x))
        h2_relu = self.relu(self.linear2(h1_relu))
        output = self.linear3(h2_relu)
        return output


model = ThreeLayerNet(D_in, H, D_out)
loss_fn = torch.nn.NLLLoss()
output_fn = F.log_softmax

# Use the optim package to define an Optimizer that will update the weights of
# the model for us. Here we will use Adam; the optim package contains many other
# optimization algoriths. The first argument to the Adam constructor tells the
# optimizer which Tensors it should update.
learning_rate = 1e-3
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
for t in range(1500):
    # Forward pass: compute predicted y by passing x to the model.
    output = model(x)
    y_pred = output_fn(output, dim=0)

    # Compute and print loss.
    loss = loss_fn(y_pred, y)
    if t % 10 == 0:
        print 'Epoch: {}, Loss: {}'.format(t, loss.item())
        acc = torch.sum(torch.argmax(y_pred, 1) == y).item() * 1.0 / y.shape[0]
        print 'Accuracy : {}'.format(acc)

    # Before the backward pass, use the optimizer object to zero all of the
    # gradients for the variables it will update (which are the learnable
    # weights of the model). This is because by default, gradients are
    # accumulated in buffers( i.e, not overwritten) whenever .backward()
    # is called. Checkout docs of torch.autograd.backward for more details.
    optimizer.zero_grad()

    # Backward pass: compute gradient of the loss with respect to model
    # parameters
    loss.backward()

    # Calling the step function on an Optimizer makes an update to its
    # parameters
    optimizer.step()


