# -*- coding: utf-8 -*-
import random
import torch

N, D_in = 64, 1000

x = torch.randn(N, D_in)
y = torch.randn(N, D_in)


loss = (x - y).pow(2).sum().item()

print loss

